import { LocationService } from './../../shared/services/location.service';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.scss']
})
export class LocationComponent implements OnInit {

  location;

  constructor(private locationService: LocationService) { }

  ngOnInit(): void {
    this.locationService.getDataSimple().subscribe( (res: any) => {
      this.location = res.results;
      console.log('##ABEL## >> AppComponent >>  res', res.results);
    });

  }

}
