import { FavoritesCharactersService } from './../../shared/services/favorites-characters.service';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-favourite-characters',
  templateUrl: './favourite-characters.component.html',
  styleUrls: ['./favourite-characters.component.scss']
})
export class FavouriteCharactersComponent implements OnInit {

  favoritesCharacters;

  constructor(private favoritesCharactersService: FavoritesCharactersService) { }

  ngOnInit(): void {
    this.favoritesCharacters = this.favoritesCharactersService.getFavoritesCharacters();
  }

}
