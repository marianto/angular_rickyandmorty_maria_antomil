import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FavouriteCharactersComponent } from './favourite-characters.component';

describe('FavouriteCharactersComponent', () => {
  let component: FavouriteCharactersComponent;
  let fixture: ComponentFixture<FavouriteCharactersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FavouriteCharactersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FavouriteCharactersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
