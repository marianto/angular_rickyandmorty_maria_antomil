import { CharactersService } from './../../shared/services/characters.service';
import { Component, OnInit } from '@angular/core';



@Component({
  selector: 'app-character-simple-page',
  templateUrl: './character-simple-page.component.html',
  styleUrls: ['./character-simple-page.component.scss']
})
export class CharacterSimplePageComponent implements OnInit {

  characters;
  // tslint:disable-next-line: max-line-length
  constructor(private charactersService: CharactersService) {}
  // tslint:disable-next-line: typedef
  ngOnInit(){
    this.charactersService.getDataSimple().subscribe( (res: any) => {
      console.log('##ABEL## >> AppComponent >>  res', res.results);
      this.characters = res.results;
    });

}
}
