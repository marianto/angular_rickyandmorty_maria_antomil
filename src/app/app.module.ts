import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule} from '@angular/common/http';
import { GalleryComponent } from './shared/services/gallery/gallery.component';
import { PagesComponent } from './pages/pages.component';
import { CharacterSimplePageComponent } from './pages/character-simple-page/character-simple-page.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { MenuComponent } from './shared/services/menu/menu.component';
import { LocationComponent } from './pages/location/location.component';
import { FavouriteCharactersComponent } from './pages/favourite-characters/favourite-characters.component';


@NgModule({
  declarations: [
    AppComponent,
    GalleryComponent,
    PagesComponent,
    CharacterSimplePageComponent,
    HomePageComponent,
    MenuComponent,
    LocationComponent,
    FavouriteCharactersComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
