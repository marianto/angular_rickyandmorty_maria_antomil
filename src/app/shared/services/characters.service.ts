import { environment } from './../../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { UrlHandlingStrategy } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class CharactersService {

  constructor(private httpClient: HttpClient) { }

  // tslint:disable-next-line: typedef
  getDataSimple(){
    return this.httpClient.get( environment.url + 'character/' );
  }
}
