import { Injectable } from '@angular/core';
import { environment } from './../../../environments/environment';
import { HttpClient} from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class LocationService {

  constructor(private httpClient: HttpClient) {}
  // tslint:disable-next-line: typedef
  getDataSimple() {
    return this.httpClient.get('https://rickandmortyapi.com/api/location/');
  }

}
