import { CharactersService } from './../characters.service';
import { FavoritesCharactersService } from './../favorites-characters.service';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {

 @Input () characters;



  constructor(private favoritesCharactersService = FavoritesCharactersService) { }

  ngOnInit(): void {
  }

  // tslint:disable-next-line: typedef
  addNewFavoriteCharacter(character){
    this.favoritesCharactersService.addNewFavoriteCharacter(character)
  }

}
