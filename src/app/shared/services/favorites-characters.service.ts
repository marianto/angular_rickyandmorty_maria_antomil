import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { environment } from './../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FavoritesCharactersService {

  private favoritesCharacters = [];

  constructor() { }

  // tslint:disable-next-line: typedef- con esta funcion añadimos nuevos personajes//
  // tslint:disable-next-line: typedef
  addNewFavoriteCharacter(newFavoriteCharacter){
    this.favoritesCharacters.push(newFavoriteCharacter);
  }

  // tslint:disable-next-line: typedef//con esta funcion nos hacemos un get para recuperar el valor//
  // tslint:disable-next-line: typedef
  getFavoritesCharacters() {
    return this.favoritesCharacters;
  }

}
