import { LocationService } from './shared/services/location.service';
import { CharactersService } from './shared/services/characters.service';
import { Component, OnInit } from '@angular/core';
import { HttpClient} from '@angular/common/http';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent{
    title = 'rick-and-morty';
    characters;
    location;
    results: any;


    constructor(private charactersService: CharactersService, private httpClient: HttpClient) {}

// tslint:disable-next-line: typedef

}

