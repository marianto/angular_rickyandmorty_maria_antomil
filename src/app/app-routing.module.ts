import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CharacterSimplePageComponent } from './pages/character-simple-page/character-simple-page.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { MenuComponent } from './shared/services/menu/menu.component';
import { LocationComponent } from './pages/location/location.component';
import { FavouriteCharactersComponent } from './pages/favourite-characters/favourite-characters.component';

const routes: Routes = [{
  path: '', component: HomePageComponent,
},
{
  path: 'characters', component: CharacterSimplePageComponent,
},
{
  path: 'location', component: LocationComponent,
},
{
  path: 'favourites', component: FavouriteCharactersComponent,
},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
